(define-library (json-rpc lolevel)

(export json-rpc-exit
        json-rpc-handler-table
        json-rpc-log-level
        json-rpc-loop
        json-rpc-read
        json-rpc-write

        custom-error-codes
        make-json-rpc-custom-error
        make-json-rpc-internal-error
        make-json-rpc-invalid-request-error
        json-rpc-error?
        json-rpc-custom-error?
        json-rpc-invalid-request-error?
        json-rpc-internal-error?)

(import (scheme base)
        (scheme char))

(cond-expand
 (chicken
  (import (chicken base)
          (chicken port)
          medea
          scheme
          srfi-1
          srfi-13
          srfi-28
          srfi-69))
 (else))

(include "lolevel.scm"))
