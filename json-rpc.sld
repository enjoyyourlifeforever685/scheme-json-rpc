(define-module (json-rpc)

#:export (json-rpc-call
          json-rpc-call/tcp
          json-rpc-start-server/tcp)

#:re-export (json-rpc-exit
             json-rpc-handler-table
             json-rpc-log-level)

#:use-module (scheme base)
#:use-module (scheme char)
#:use-module (srfi srfi-1)
#:use-module (srfi srfi-13)
#:use-module (srfi srfi-28)
#:use-module (srfi srfi-69)
#:use-module (srfi srfi-145)
#:use-module (srfi srfi-180)
#:use-module (json-rpc lolevel)

#:declarative? #f)

;; ignored for now
(define tcp-read-timeout (make-parameter #f))

(define (tcp-listen tcp-port)
  (define sock (socket PF_INET SOCK_STREAM 0))
  (setsockopt sock SOL_SOCKET SO_REUSEADDR 1)
  (bind sock (make-socket-address AF_INET INADDR_LOOPBACK tcp-port))
  (listen sock 20)
  sock)

(define (tcp-accept listener)
  (define res (accept listener))
  (define port (car res))
  (values port port))

(define (tcp-connect tcp-address tcp-port)
  (define sock (socket PF_INET SOCK_STREAM 0))
  (setsockopt sock SOL_SOCKET SO_REUSEADDR 1)
  (connect sock (make-socket-address AF_INET tcp-address tcp-port)))

(define (tcp-close conn)
  (close conn))

(include "json-rpc/json-rpc.scm")
